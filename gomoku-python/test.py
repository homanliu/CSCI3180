import abc

class Plugin(object):
    __metaclass__ = abc.ABCMeta
    @abc.abstractmethod
    def run(self):
        return

class Greeting(Plugin):
    def run(self):
        print "All the best!"

greetingObj = Greeting()
greetingObj.run()