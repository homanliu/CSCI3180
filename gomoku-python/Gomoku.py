import abc
import random

class Gomoku(object):
    def __init__(self):
        # Create two players and initialize other instance variables
        self.gameboard = []
        for i in range(9):
            self.gameboard.append([0, 0, 0, 0, 0, 0, 0, 0, 0])
        self.turn = 1
    
    def createPlayer(self, symbol, playerNum):
        # Create a player (human- or computer-controlled) 
        # with corresponding symbol ('O' or 'X')
        # and player number (1 or 2) by prompting the user
        if (symbol == 'O'):
            if (playerNum == 1):
                self.player1 = Human(symbol, self.gameboard)
            elif (playerNum == 2):
                self.player1 = Computer(symbol, self.gameboard)
        elif (symbol == 'X'):
            if (playerNum == 1):
                self.player2 = Human(symbol, self.gameboard)
            elif (playerNum == 2):
                self.player2 = Computer(symbol, self.gameboard)
        return
        
    def startGame(self):
        endGame = False
        # Start a new game and play until winning/losing or draw
        while not endGame:
            if (self.turn % 2 == 1):
                print 'Player O\'s turn!'
                self.player1.nextMove()
            else:
                print 'Player X\'s turn!'
                self.player2.nextMove()
            self.turn += 1
            self.printGameBoard()
            playerWin = self.checkWin()
            tieGame = self.checkTie()
            endGame = playerWin[0] or playerWin[1] or tieGame or endGame
            if playerWin[0]:
                print 'Player 1 wins'
            elif playerWin[1]:
                print 'Player 2 wins'
            if tieGame:
                print 'Tie'
        return
      
    def printGameBoard(self):
        # Print out the game board in the command line window
        header = '   |'
        separator = '----------------------------------------'
        for i in range(9):
            header += ' ' + str(i + 1) + ' |'
        print header
        print separator
        for row, lists in enumerate(self.gameboard):
            rowDisplay = ' ' + str(row + 1) + ' |'
            for col, element in enumerate(lists):
                if (element == 0):
                    rowDisplay += '   |'
                else:
                    rowDisplay += ' ' + str(element) + ' |'
            print rowDisplay
            print separator
        print ''
        return

    def checkWin(self):
        playersWin = [False, False]
        # Check if any player has won the game
        # Check horizontal consecutive
        for lists in self.gameboard:
            count = 0
            current = ''
            for element in lists:
                if (current != element):
                    current = element
                    count = 1
                elif (current == element):
                    count += 1
                if (count == 5):
                    if (current == 'O'):
                        playersWin[0] = True
                    elif (current == 'X'):
                        playersWin[1] = True
        # ===========================

        # Check vertical consecutive
        for j in range(9):
            count = 0
            current = ''
            for i in range(9):
                if (current != self.gameboard[i][j]):
                    current = self.gameboard[i][j]
                    count = 1
                elif (current == self.gameboard[i][j]):
                    count += 1
                if (count == 5):
                    if (current == 'O'):
                        playersWin[0] = True
                    elif (current == 'X'):
                        playersWin[1] = True
        # ===========================

        # Check oblique consective
        # For (1, 1) to (5, 5), next grid = (+1, +1)
        for i in range(5):
            count = 0
            current = ''
            for j in range(5):
                if (current != self.gameboard[i + j][i + j]):
                    current = self.gameboard[i + j][i + j]
                    count = 1
                elif (current == self.gameboard[i + j][i + j]):
                    count += 1
                if (count == 5):
                    if (current == 'O'):
                        playersWin[0] = True
                    elif (current == 'X'):
                        playersWin[1] = True
        # For (1, 9) to (5, 5), next grid = (+1, -1)
        for i in range(5):
            count = 0
            current = ''
            for j in range(5):
                if (current != self.gameboard[i + j][8 - i - j]):
                    current = self.gameboard[i + j][8 - i - j]
                    count = 1
                elif (current == self.gameboard[i + j][8 - i - j]):
                    count += 1
                if (count == 5):
                    if (current == 'O'):
                        playersWin[0] = True
                    elif (current == 'X'):
                        playersWin[1] = True
        # For (9, 9) to (5, 5), next grid = (-1, -1)
        for i in range(5):
            count = 0
            current = ''
            for j in range(5):
                if (current != self.gameboard[8 - i - j][8 - i - j]):
                    current = self.gameboard[8 - i - j][8 - i - j]
                    count = 1
                elif (current == self.gameboard[8 - i - j][8 - i - j]):
                    count += 1
                if (count == 5):
                    if (current == 'O'):
                        playersWin[0] = True
                    elif (current == 'X'):
                        playersWin[1] = True
        # For (9, 1) to (5, 5), next grid = (-1, +1)
        for i in range(5):
            count = 0
            current = ''
            for j in range(5):
                if (current != self.gameboard[8 - i - j][i + j]):
                    current = self.gameboard[8 - i - j][i + j]
                    count = 1
                elif (current == self.gameboard[8 - i - j][i + j]):
                    count += 1
                if (count == 5):
                    if (current == 'O'):
                        playersWin[0] = True
                    elif (current == 'X'):
                        playersWin[1] = True

        return playersWin
        
    def checkTie(self):
        # Check if the game is ending in a tie
        for lists in self.gameboard:
            for element in lists:
                if (element == 0):
                    return False
        return True

class Player(object):
    __metaclass__ = abc.ABCMeta
    @abc.abstractmethod
    def __init__(self, symbol, gameboard):
        # Initialize the player with its symbol 'X' or 'O' and corresponding gameboard
        self.symbol = symbol
        self.gameboard = gameboard
        return
    
    def nextMove(self):
        return

class Human(Player):
    def __init__(self, symbol, gameboard):
        super(Human, self).__init__(symbol, gameboard)
        return
    
    def nextMove(self):
        validMove = False
        while not validMove:
            inputString = raw_input('Type the row and col to put the disc: ')
            if (len(inputString) < 3):
                print 'Invalid input!'
                continue
            row = ord(inputString[0]) - 48
            col = ord(inputString[2]) - 48
            if (row < 1 or row > 9 or col < 1 or col > 9):
                print 'Invalid input!'
                continue
            if (self.gameboard[row - 1][col - 1] != 0):
                print 'Invalid input!'
                continue
            self.gameboard[row - 1][col - 1] = self.symbol
            validMove = True
        return

class Computer(Player):
    def __init__(self, symbol, gameboard):
        super(Computer, self).__init__(symbol, gameboard)
        return

    def nextMove(self):
        while True:
            index_i = random.randint(0, 8)
            index_j = random.randint(0, 8)
            if(self.gameboard[index_i][index_j] == 0):
                self.gameboard[index_i][index_j] = self.symbol
                break
        return

def choosePlayer(playerNum):
    chooseStr = 'Please choose Player ' + str(playerNum)
    if (playerNum == 1):
        chooseStr += ' (O) :'
    else:
        chooseStr += ' (X) :'
    print chooseStr
    print '1. Human'
    print '2. Computer Player'
    num = 0
    while(num != 1 and num != 2):
        num = input('Your choice is: ')
    if (num == 1 and playerNum == 1):
        print 'Player O is Human.'
    elif (num == 2 and playerNum == 1):
        print 'Player O is Computer.'
    elif (num == 1 and playerNum == 2):
        print 'Player X is Human.'
    elif (num == 2 and playerNum == 2):
        print 'Player X is Computer.'
    return num

gameObject = Gomoku()
gameObject.createPlayer('O', choosePlayer(1))
gameObject.createPlayer('X', choosePlayer(2))
gameObject.printGameBoard()
gameObject.startGame()
