# CSCI3180 Principles of Programming Languages
#
# --- Declaration ---
#
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
#
# Assignment 2
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

import math

class Pos(object):
    def __init__(self, x, y):
        self.__x = x
        self.__y = y
        return

    def distance(self, *args):
        if (type(args[0]) is int):
            return abs(self.__x - args[0]) + abs(self.__y - args[1])
        else:
            return abs(self.__x - args[0].x) + abs(self.__y - args[0].y)
    
    def setPos(self, x, y):
        self.__x = x
        self.__y = y
        return
    
    def getX(self):
        return self.__x
    
    def getY(self):
        return self.__y