# CSCI3180 Principles of Programming Languages
#
# --- Declaration ---
#
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
#
# Assignment 2
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

from __future__ import print_function
import abc
import random
import math

from Obstacle import Obstacle
from Player import Player
from Human import Human
from Chark import Chark

class SurvivalGame(object):
    D = 10
    __O = 2
    n = 0
    __teleportObjects = []

    def __init__(self):
        return
    
    def printBoard(self):
        printObject = []
        for i in range(self.D):
            printObject.append([' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '])
        
        for i in range(self.n):
            pos = self.__teleportObjects[i].getPos()
            printObject[pos.getX()][pos.getY()] = self.__teleportObjects[i].getName()

        for i in range(self.__O):
            pos = self.__teleportObjects[i + self.n].getPos()
            printObject[pos.getX()][pos.getY()] = 'O' + str(i)

        # print('.', end='')
        print (' ', end='')
        for i in range(self.D):
            print ('| {}  '.format(i), end='')
        print ('|')
        for i in range(int(self.D * 5.5)):
            print ('-', end='')
        print ('')
        for row in range(self.D):
            print (row, end='')
            for col in range(self.D):
                if printObject[row][col] is ' ':
                    print ('|    ', end='')
                else:
                    print ('| {} '.format(printObject[row][col]), end='')
            print ('|')
            for i in range(int(self.D * 5.5)):
                print ('-', end='')
            print ('')
        return
    
    def positionOccupied(self, randx, randy):
        for obj in self.__teleportObjects:
            pos = obj.getPos()
            if (pos.getX() == randx and pos.getY() == randy):
                return True
        return False
    
    def getPlayer(self, randx, randy):
        for obj in self.__teleportObjects:
            if isinstance(obj, Player):
                pos = obj.getPos()
                if (pos.getX() == randx and pos.getY() == randy):
                    return obj
        return None
    
    def init(self):
        print ('Welcome to Kafustrok. Light blesses you. \nInput number of players: (a even number)')
        self.n = input()
        self.__teleportObjects = [None] * (self.n + self.__O)

        for i in range(self.n / 2):
            self.__teleportObjects[i] = Human(0, 0, i, self)
            self.__teleportObjects[i + self.n / 2] = Chark(0, 0, i, self)
        
        for i in range(self.__O):
            self.__teleportObjects[i + self.n] = Obstacle(0, 0, i, self)

        return
    
    def gameStart(self):
        turn = 0
        numOfAlivePlayers = self.n
        while (numOfAlivePlayers > 1):
            if (turn == 0):
                for obj in self.__teleportObjects:
                    obj.teleport()
                print ('Everything gets teleported..')
            self.printBoard()
            # t can move only if he is alive!
            t = self.__teleportObjects[turn]
            if (t._health > 0):
                t.askForMove()
                print ('')
            turn = (turn + 1) % self.n
            # count number of alive players
            numOfAlivePlayers = 0

            for obj in self.__teleportObjects:
                if issubclass(type(obj), Player):
                    if obj._health > 0:
                        numOfAlivePlayers += 1
        print ('Game over.')
        self.printBoard()
        return

def main():
    game = SurvivalGame()
    game.init()
    game.gameStart()


main()