# CSCI3180 Principles of Programming Languages
#
# --- Declaration ---
#
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
#
# Assignment 2
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

from __future__ import print_function

from Player import Player
from Axe import Axe

class Chark(Player):
    def __init__(self, posx, posy, index, game):
        super(Chark, self).__init__(100, 4, posx, posy, index, game)
        self._myString = 'C' + str(index)
        self._equipment = Axe(self)
        return
    
    def teleport(self):
        super(Chark, self).teleport()
        self._equipment.enhance()
        return
    
    def askForMove(self):
        print ('You are a Chark (C{}) using Axe. (Range: {}, Damage: {})'.format(
            self._index, self._equipment.getRange(), self._equipment.getEffect()))
        super(Chark, self).askForMove()
        return