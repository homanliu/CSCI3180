public class Wand {
	private final int range;
	private int effect;
    private Player owner;
    
    private static final int WAND_RANGE = 5;
	private static final int WAND_INIT_EFFECT = 10;
	
	protected Wand(Player owner) {
		this.range = WAND_RANGE;
		this.effect = WAND_INIT_EFFECT;
		this.owner = owner;
	}
	
	public void action(int posx, int posy) {
		// TODO Auto-generated method stub
		System.out.println("You are using wand healing " + posx + " " + posy + ".");

		if (this.owner.pos.distance(posx, posy)  <= this.range) {
			// search for all targets with target coordinates.
			Player player = owner.game.getPlayer(posx, posy);

			if(player != null ) 
			{
                String healingPlayer = player.getName();
                String ownerPlayer = this.owner.getName();
                if(healingPlayer.charAt(0) == ownerPlayer.charAt(0))
                    player.increaseHealth(this.effect);
                else
                    System.out.println("You cannot heal opposite race.");
			}
		} else {
			System.out.println("Out of reach.");
		}

    }
    
	public void enhance() {
		this.effect += 5;
	}

	public int getEffect() {
		return this.effect;
	}

	public int getRange() {
		return this.range;
	}
}
