# CSCI3180 Principles of Programming Languages
#
# --- Declaration ---
#
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
#
# Assignment 2
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

from __future__ import print_function

from Weapon import Weapon

class Axe(Weapon):
    __AXE_RANGE = 1
    __AXE_INIT_DAMAGE = 40

    def __init__(self, owner):
        super(Axe, self).__init__(self.__AXE_RANGE, self.__AXE_INIT_DAMAGE, owner)
        return
    
    def enhance(self):
        self._effect += 10
        return
    
    def action(self, posx, posy):
        print ('You are using axe attacking ' + str(posx) + ' ' + str(posy) + '.')
        if (self._owner._pos.distance(posx, posy) <= self._range):
            player = self._owner._game.getPlayer(posx, posy)
            if player is not None:
                player.decreaseHealth(self._effect)
            else:
                print ('Out of reach.')
        return
