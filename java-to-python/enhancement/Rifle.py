# CSCI3180 Principles of Programming Languages
#
# --- Declaration ---
#
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
#
# Assignment 2
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

from __future__ import print_function

from Weapon import Weapon

class Rifle(Weapon):
    __RIFLE_RANGE = 4
    __RIFLE_INIT_DAMAGE = 10
    __AMMO_LIMIT = 6
    __AMMO_RECHARGE = 3

    def __init__(self, owner):
        super(Rifle, self).__init__(self.__RIFLE_RANGE, self.__RIFLE_INIT_DAMAGE, owner)
        self.__ammo = self.__AMMO_LIMIT
        return
    
    def enhance(self):
        self.__ammo = min(self.__AMMO_LIMIT, self.__ammo + self.__AMMO_RECHARGE)
        return
    
    def action(self, posx, posy):
        print ('You are using rifle attacking ' + str(posx) + ' ' + str(posy) + '.')
        print ('Type how many ammos you want to use.')
        ammoToUse = input()
        if (ammoToUse > self.__ammo):
            print ('You don\'t have that ammos.')
            return
        if (self._owner._pos.distance(posx, posy) <= self._range):
            player = self._owner._game.getPlayer(posx, posy)
            if player is not None:
                player.decreaseHealth(self._effect * ammoToUse)
                self.__ammo -= ammoToUse
        else:
            print ('Out of reach.')
        return
    
    def getAmmo(self):
        return self.__ammo