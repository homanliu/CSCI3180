# CSCI3180 Principles of Programming Languages
#
# --- Declaration ---
#
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
#
# Assignment 2
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

from __future__ import print_function

from Player import Player
from Rifle import Rifle
from Wand import Wand

class Human(Player):
    def __init__(self, posx, posy, index, game):
        super(Human, self).__init__(80, 2, posx, posy, index, game)
        self._myString = 'H' + str(self._index)
        self._equipment = Rifle(self)
        return
    
    def teleport(self):
        super(Human, self).teleport()
        self._equipment.enhance()
        return
    
    def askForMove(self):
        if isinstance(self._equipment, Rifle):
            print ('You are a human (H{}) using Rifle. (Range {}, Ammo #: {}, Damage per shot: {})'.format(
                self._index, self._equipment.getRange(), self._equipment.getAmmo(), self._equipment.getEffect()))
        elif isinstance(self._equipment, Wand):
            print ('You are a human (H{}) using Wand. (Range {}, Recover per Heal: {})'.format(
                self._index, self._equipment.getRange(), self._equipment.getEffect()))
        super(Human, self).askForMove()
    
    def setEquipment(self, equipment):
        self._equipment = equipment
