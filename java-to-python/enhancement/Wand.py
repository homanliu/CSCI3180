# CSCI3180 Principles of Programming Languages
#
# --- Declaration ---
#
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
#
# Assignment 2
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

from __future__ import print_function
import abc

class Wand(object):
    __WAND_RANGE = 5
    __WAND_INIT_EFFECT = 10

    def __init__(self, owner):
        self._range = self.__WAND_RANGE
        self._effect = self.__WAND_INIT_EFFECT
        self._owner = owner
        return
    
    def action(self, posx, posy):
        print ('You are using wand healing ' + str(posx) + ' ' + str(posy) + '.')
        if (self._owner._pos.distance(posx, posy) <= self._range):
            player = self._owner._game.getPlayer(posx, posy)
            if player is not None:
                healingPlayer = player.getName()
                ownerPlayer = self._owner.getName()
                print ('Healing: ' + healingPlayer + ', Owner: ' + ownerPlayer)
                if healingPlayer[0] == ownerPlayer[0]:
                    player.increaseHealth(self._effect)
                else:
                    print ('You cannot heal opposite race.')
            else:
                print ('Out of reach.')
        return
    
    def enhance(self):
        self._effect += 5
        return
    
    def getEffect(self):
        return self._effect
    
    def getRange(self):
        return self._range
