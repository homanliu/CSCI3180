# CSCI3180 Principles of Programming Languages
#
# --- Declaration ---
#
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
#
# Assignment 2
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

from __future__ import print_function
import abc
import random

from Pos import Pos

class Player(object):
    __metaclass__ = abc.ABCMeta
    __MOBILITY = None
    _pos = None
    _health = None
    _equipment = None
    _index = None
    _myString = None
    _game = None
    @abc.abstractmethod

    def __init__(self, healthCap, mob, posx, posy, index, game):
        self.__MOBILITY = mob
        self._health = healthCap
        self._pos = Pos(posx, posy)
        self._index = index
        self._game = game
        return
    
    def getPos(self):
        return self._pos
    
    def teleport(self):
        randx = random.randint(0, self._game.D - 1)
        randy = random.randint(0, self._game.D - 1)
        while (self._game.positionOccupied(randx, randy)):
            randx = random.randint(0, self._game.D - 1)
            randy = random.randint(0, self._game.D - 1)
        self._pos.setPos(randx, randy)
        return
    
    def increaseHealth(self, h):
        self._health += h
        if (self._health > 0):
            self._myString = self._myString[1] + str(self._index)
        return
    
    def decreaseHealth(self, h):
        self._health -= h
        if (self._health <= 0):
            self._myString = 'C' + self._myString[0]
        return
    
    def getName(self):
        return self._myString
    
    def askForMove(self):
        print ('Your health is ' + str(self._health) + '. Your position is ({}, {}). Your mobility is {}.'.format(
            self._pos.getX(), self._pos.getY(), self.__MOBILITY))
        print ('You now have following options: ')
        print ('1. Move')
        print ('2. Attack')
        print ('3. End tne turn')

        a = input()
        if (a == 1):
            print ('Specify your target position. (Input \'x y\')')
            posx, posy = [int(s) for s in raw_input().split()]
            if (self._pos.distance(posx, posy) > self.__MOBILITY):
                print ('Beyond your reach. Staying still.')
            elif (self._game.positionOccupied(posx, posy)):
                print ('Position occupied. Cannot move there.')
            else:
                self._pos.setPos(posx, posy)
                self._game.printBoard()
                print ('You can now \n1.attack\n2.End the turn')
                inputNum = input()
                if (inputNum == 1):
                    print ('Input position to attack. (Input \'x y\')')
                    attx, atty = [int(s) for s in raw_input().split()]
                    self._equipment.action(attx, atty)
        elif (a == 2):
            print ('Input position to attack. (Input \'x y\')')
            attx, atty = [int(s) for s in raw_input().split()]
            self._equipment.action(attx, atty)
        elif (a == 3):
            return
        return
