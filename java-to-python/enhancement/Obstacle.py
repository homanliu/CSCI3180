# CSCI3180 Principles of Programming Languages
#
# --- Declaration ---
#
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
#
# Assignment 2
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

import random
from Pos import Pos

class Obstacle(object):
    def __init__(self, posx, posy, index, game):
        self.__pos = Pos(posx, posy)
        self.index = index
        self.__game = game
        return
    
    def getPos(self):
        return self.__pos
    
    def teleport(self):
        randx = random.randint(0, self.__game.D - 1)
        randy = random.randint(0, self.__game.D - 1)
        while (self.__game.positionOccupied(randx, randy)):
            randx = random.randint(0, self.__game.D - 1)
            randy = random.randint(0, self.__game.D - 1)
        self.__pos.setPos(randx, randy)
        return