(* CSCI3180 Principles of Programming Languages

--- Declaration ---

I declare that the assignment here submitted is original except for source
material explicitly acknowledged. I also acknowledge that I am aware of
University policy and regulations on honesty in academic work, and of the
disciplinary guidelines and procedures applicable to breaches of such policy
and regulations, as contained in the website
http://www.cuhk.edu.hk/policy/academichonesty/

Assignment 4
Name : Liu Ho Man
Student ID : 1155077343
Email Addr : hmliu6@cse.cuhk.edu.hk *)

val task = [("t1", 2, 4), ("t2", 4, 6)];

val tasks = [
  ("t1", 2, 4),
  ("t2", 4, 6),
  ("t3", 3, 7),
  ("t4", 8, 9),
  ("t5", 1, 10),
  ("t6", 10, 1)
];

fun compare_string(name1: string, name2: string) = 
    if name1 = name2 then true else false;

fun check_task(T: string, tasklist: (string * int * int) list): bool = 
    let val (name, start_time, end_time) = hd(tasklist)
        val remain = tl(tasklist)
    in
        if length(tasklist) = 0 then false
        else if compare_string(name, T) = true andalso end_time >= start_time then true
        else if compare_string(name, T) = true andalso end_time < start_time then false
        else if length(tasklist) > 1 then check_task(T, remain)
        else false
    end;

fun get_task(taskname: string, tasklist: (string * int * int) list): (string * int * int) = 
    let val (name, start_time, end_time) = hd(tasklist)
    in
        if name = taskname andalso end_time >= start_time then (name, start_time, end_time)
        else if length(tasklist) = 1 then ("null", 0, 0)
        else get_task(taskname, tl(tasklist))
    end;

fun compatible(T1: string, T2: string, tasklist: (string * int * int) list): bool = 
    if length(tasklist) = 0 then false
    else
        let val (t1_name, t1_start, t1_end) = get_task(T1, tasklist)
            val (t2_name, t2_start, t2_end) = get_task(T2, tasklist)
        in
            if t1_name = "null" orelse t2_name = "null" then false
            else if t2_start >= t1_end orelse t1_start >= t2_end then true
            else false
        end;

fun compatible_order(T1: string, T2: string, tasklist: (string * int * int) list): (string * string) = 
    if length(tasklist) = 0 then ("null", "null")
    else
        let val (t1_name, t1_start, t1_end) = get_task(T1, tasklist)
            val (t2_name, t2_start, t2_end) = get_task(T2, tasklist)
        in
            if t1_name = "null" orelse t2_name = "null" then ("null", "null")
            else if t2_start >= t1_end then (t1_name, t2_name)
            else if t1_start >= t2_end then (t2_name, t1_name)
            else ("null", "null")
        end;

fun get_tasklist(L: string list, tasklist: (string * int * int) list): (string * int * int) list = 
    let val remain = tl(L)
        val return_task = get_task(hd(L), tasklist)
    in
        if length(L) > 1 then return_task :: get_tasklist(tl(L), tasklist)
        else return_task :: []
    end;

fun compatible_list_element(T: (string * int * int), L: (string * int * int) list): bool = 
    let val (t2_name, t2_start, _) = hd(L)
        val (t1_name, _, t1_end) = T
        val remain_L = tl(L)
    in
        if length(L) > 1 andalso t1_end <= t2_start andalso t1_name <> t2_name then compatible_list_element(T, remain_L)
        else if length(L) = 1 andalso t1_end <= t2_start andalso t1_name <> t2_name then true
        else if t1_name = t2_name then compatible_list_element(T, remain_L)
        else false
    end;

fun all_compatible(L_check: (string * int * int) list): bool =
    let val first_element_test = compatible_list_element(hd(L_check), L_check)
    in
        if length(L_check) > 2 andalso first_element_test then all_compatible(tl(L_check))
        else if length(L_check) = 2 andalso first_element_test then true
        else false
    end;

fun compatible_list(L: string list, tasklist: (string * int * int) list): bool = 
    let val L_list = get_tasklist(L, tasklist)
    in
        all_compatible(L_list)
    end;

(*)
(* True *)
check_task("t1", task);
(* False *)
check_task("t3", task);

get_task("t1", task);
get_task("t2", task);

(* True *)
compatible("t1", "t2", task);
compatible("t3", "t4", tasks);
(* False *)
compatible("t5", "t6", tasks);
compatible("t2", "t3", tasks);

(* True *)
compatible_list_element(get_task("t1", tasks), get_tasklist(["t1", "t2", "t4"], tasks));
(* False *)
compatible_list_element(get_task("t2", tasks), get_tasklist(["t1", "t2", "t4"], tasks));
compatible_list_element(get_task("t4", tasks), get_tasklist(["t1", "t2", "t4"], tasks));

(* True *)
compatible_list(["t1", "t2", "t4"], tasks);
(* False *)
compatible_list(["t1", "t4", "t2"], tasks);
compatible_list(["t1", "t4", "t3"], tasks);
*)