(* CSCI3180 Principles of Programming Languages

--- Declaration ---

I declare that the assignment here submitted is original except for source
material explicitly acknowledged. I also acknowledge that I am aware of
University policy and regulations on honesty in academic work, and of the
disciplinary guidelines and procedures applicable to breaches of such policy
and regulations, as contained in the website
http://www.cuhk.edu.hk/policy/academichonesty/

Assignment 4
Name : Liu Ho Man
Student ID : 1155077343
Email Addr : hmliu6@cse.cuhk.edu.hk *)

(* 1(a).
Normal Order Reduction:
    (λc.c (λa.λb.b))((λa.λb.λf.f a b) p q)
=>β ((λa.λb.λf.f a b) p q)(λa.λb.b)
=>β ((λb.λf.f p b) q)(λa.λb.b)
=>β (λf.f p q)(λa.λb.b)
=>β (λa.λb.b p q)
=>β (λb.b q)
=>β q

1(b).
Applicative Order Reduction:
    (λc.c (λa.λb.b))((λa.λb.λf.f a b) p q)
=>β (λc.c (λa.λb.b))((λb.λf.f p b) q)
=>β (λc.c (λa.λb.b))(λf.f p q)
=>β (λf.f p q)(λa.λb.b)
=>β (λa.λb.b p q)
=>β (λb.b q)
=>β q

2(a).
Normal Order Reduction:
    (λx.λy. (mul x ((λx. (add x 3)) y))) 7 8
=>α (λw.λy. (mul w ((λx. (add x 3)) y))) 7 8
=>β (λy. (mul 7 ((λx. (add x 3)) y))) 8
=>β (mul 7 ((λx. (add x 3)) 8))
=>β (mul 7 ((λx. (add x 3)) 8))
=>β (mul 7 (add 8 3))
=>δ (mul 7 11)
=>δ 77

2(b).
Applicative Order Reduction:
    (λx.λy. (mul x ((λx. (add x 3)) y))) 7 8
=>α (λx.λy. (mul x ((λw. (add w 3)) y))) 7 8
=>β (λx.λy. (mul x (add y 3))) 7 8
=>β (λy. (mul 7 (add y 3))) 8
=>β (mul 7 (add 8 3))
=>δ (mul 7 11)
=>δ (mul 7 11)
=>δ 77 *)

(* 2(c). *)
fun mul x y = x * y;

fun add x y = x + y;

fun t x = (add x 3);

fun f x y = (mul x (t y));

(* 3(a).
Normal Order Reduction:
k =   λx.(add x 1) m
  =>β (add m 1)
  =>δ 2
1st printf(k) => 2

Applicative Order Reduction:
m =   (λ g.g 5) (λ x.(x + 3))
  =>β (λ x.(x + 3)) 5
  =>β (5 + 3)
  =>δ 8
2nd printf(m) => 8

3rd printf(k) => 8

4th printf(k, m) => 5, 11

3(b).
I agree with Peter since both normal order and applicative order reduction will return same normal form  
by the Church-Rosser Theorem. Therefore, it is not neccessary to specify the evaluation strategy.

3(c).
variable t, x: dynamic scoping, lifetime of active subprogram swap() or f1

3(d).
I suggest to use dynamic scoping in Lapi because functional programming is based on lambda calculus 
and variables in lambda calculus can be changed by α-reduction which implied that variable name does 
not important and they should be consistent inside same function. *)
