
C       CSCI3180 Principles of Programming Languages 
C
C       --- Declaration ---
C       I declare that the assignment here submitted is original except for source
C       material explicitly acknowledged. I also acknowledge that I am aware of
C       University policy and regulations on honesty in academic work, and of the
C       disciplinary guidelines and procedures applicable to breaches of such policy
C       and regulations, as contained in the website
C       http://www.cuhk.edu.hk/policy/academichonesty/
C
C       Assignment 1
C       Name : Liu Ho Man
C       Student ID : 1155077343
C       Email Addr : hmliu6@cse.cuhk.edu.hk

      program main
        implicit none

C       Variable Declaration

C       Arguments reading
        character*30    arg1
        character*30    arg2

C       For teams.txt file reading
        integer         fileunit
        character*15    teamname
        integer         index
        logical         parsing
        integer         iostatus

C       For submission-records.txt file reading
        integer         recordFile
        character*15    currentTeam
        integer         question
        character*19    outcome
        integer         score
        integer         recordStatus

C       Submission records input
        integer         submissionTime
        integer         scoreSum
        integer         latestScore
        integer         maxScore
        integer         minScore
        integer         currentQuestion

C       Submission records output
        integer         problemScore
        integer         teamTotal
        character*93    teamScoreString
        character       questionNumber
        character*3     scoreString
        integer         stringPointer
        logical         nullTeam
        character*4     totalString

C       Text file headers output
        integer         outUnit

C       Variable Initialization
        outUnit = 20
        fileunit = 10
        recordFile = 11
        parsing = .FALSE.
        index = 1
        submissionTime = 0
        maxScore = 0
        minScore = 999
        scoreSum = 0
        currentQuestion = 0
        teamTotal = 0
        teamScoreString = ''
        stringPointer = 1
        nullTeam = .FALSE.

        call getarg(1, arg1)
        call getarg(2, arg2)
C       print*, "Arguments: ", arg1, '   ', arg2

C       File I/O
        open(fileunit, file=arg1, status='old')
        open(recordFile, file=arg2, status='old')
        open(outUnit, file='reportfor.txt'
     +  , status='unknown')
   21   format(A15, I1, A19, I3)
   11   format(A)

   51   format("2018 CUHK CSE Programming Contest\r")
   52   format("Team Score Report\r")
   53   format("\r")
        write(outUnit, 51)
        write(outUnit, 52)
        write(outUnit, 53)

   12   teamname = ''
        read(fileunit, 11, IOSTAT = iostatus) teamname
   13   call teamname_formatter(teamname, parsing, index)  
        if (.NOT. parsing) goto 13
        parsing = .FALSE.
        if (teamname .NE. '') then
C         print*, teamScoreString
          if(teamScoreString .EQ. '') goto 90
          write(outUnit, 11) teamScoreString
   90     teamScoreString = ''
          stringPointer = 0
          teamScoreString = teamname(1:15)
          stringPointer = 16
        end if
        index = 1

   22   read(recordFile, 21, IOSTAT = recordStatus) currentTeam
     +  , question, outcome, score
C       print*, currentTeam, question, outcome, score

   33   if ((currentTeam .NE. teamname .AND. .NOT. nullTeam) .OR.
     +   currentQuestion .NE. question) goto 31

        nullTeam = .FALSE.

        scoreSum = scoreSum + score
        if (score .GT. maxScore) maxScore = score
        if (score .LT. minScore) minScore = score
        latestScore = score
        submissionTime = submissionTime + 1
        goto 32

C       print*, 'Problem ', currentQuestion
   31   if (recordStatus .LT. 0) then
          submissionTime = submissionTime - 1
          scoreSum = scoreSum - score
        end if

   40   format(I1)
        write(questionNumber, 40) currentQuestion
        teamScoreString = teamScoreString(1:stringPointer - 1)//'('
     +  //questionNumber(1:1)//')'
        stringPointer = stringPointer + 3
        currentQuestion = currentQuestion + 1

        if(currentTeam .EQ. 'Splash' .AND. currentQuestion .EQ. 0)
     +    print*, 'Count: ', submissionTime
        call score_calculate(latestScore, submissionTime
     +  , 100-maxScore+minScore, maxScore, scoreSum
     +  , problemScore, teamTotal)

   41   format(I3)
        write(scoreString, 41) problemScore
        teamScoreString = teamScoreString(1:stringPointer - 1)
     +  //scoreString(1:3)//' '
        stringPointer = stringPointer + 4

   34   if (currentTeam .NE. teamname .AND. currentQuestion .LE. 9) then
C         print*, "Right Place"
C         print*, 'Problem ', currentQuestion
C         print*, 'Problem Score: ', 0
          write(questionNumber, 40) currentQuestion
          teamScoreString = teamScoreString(1:stringPointer - 1)//'('
     +    //questionNumber(1:1)//')'
          stringPointer = stringPointer + 3

          write(scoreString, 41) 0
          teamScoreString = teamScoreString(1:stringPointer - 1)
     +    //scoreString(1:3)//' '
          stringPointer = stringPointer + 4
          currentQuestion = currentQuestion + 1
          goto 34
        end if

        if (currentQuestion .EQ. 10) then
        write(totalString, 41) teamTotal
        teamScoreString = teamScoreString(1:stringPointer - 1)
     +    //'T: '//totalString(1:4)//'\r'
        stringPointer = 0
        teamTotal = 0
        end if

        scoreSum = 0
        maxScore = 0
        minScore = 999
        submissionTime = 0
        scoreSum = scoreSum + score
        if (score .GT. maxScore) maxScore = score
        if (score .LT. minScore) minScore = score
        latestScore = score
        submissionTime = submissionTime + 1
        currentQuestion = question

   32   if (recordStatus .GE. 0 .AND. currentTeam .EQ. teamname) then
          nullTeam = .TRUE.
          goto 22
        end if
        if (iostatus .GE. 0) goto 12

C       print*, teamScoreString
        write(outUnit, 11) teamScoreString
        
        close(recordFile)
        close(fileunit)
        close(outUnit)
        stop
      end

      subroutine teamname_formatter(input, indicate, index)
        implicit none
C       Variable Declaration
        character*15    input
        logical         indicate
        integer         index
        if (input(index:index) .EQ. '\r') then
          input(index:index) = ''
          indicate = .TRUE.
        end if
        if (index .EQ. len(input) .AND. .NOT. indicate) then
          indicate = .TRUE.
        end if
        index = index + 1
        return
      end

      subroutine score_calculate(baseScore, submissions
     + ,robustness, maxScore, scoreSum, problemScore, totalScore)
        implicit none
C       Variable Declaration
        integer         baseScore
        integer         submissions
        integer         decay
        integer         robustness
        integer         maxScore
        integer         problemScore
        integer         scoreSum
        integer         totalScore
C       Variable Initialization
        decay = 100.0 / submissions
        if (maxScore .LE. 30) robustness = 0

        problemScore = 0.6 * baseScore * decay / 100
     +  + 0.3 * scoreSum / submissions + 0.1 * robustness
        if (baseScore .EQ. 100) then
          problemScore = 0.6 * baseScore
     +    + 0.3 * scoreSum / submissions + 0.1 * robustness
        end if
        totalScore = totalScore + problemScore
        return
      end
