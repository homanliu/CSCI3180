C My first program
C print out hello world
      PROGRAM Helloworld
C force explicit type declarations
      IMPLICIT NONE
C variable declaration
      CHARACTER*20 STR
      STR = 'Hello world'
C program statement
      call test()
      WRITE (*,10) STR
  10  FORMAT(A,/,'This statement tells you how to continue to the next'
     + , ' line')
      STOP 
      END

      subroutine test()
      IMPLICIT NONE
      print*, 'Hello'
      end