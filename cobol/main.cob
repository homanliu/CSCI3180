      * CSCI3180 Principles of Programming Languages
      *
      * --- Declaration ---
      * I declare that the assignment here submitted is original except for source
      * material explicitly acknowledged. I also acknowledge that I am aware of
      * University policy and regulations on honesty in academic work, and of the
      * disciplinary guidelines and procedures applicable to breaches of such policy
      * and regulations, as contained in the website
      * http://www.cuhk.edu.hk/policy/academichonesty/
      *
      * Assignment 1
      * Name: Liu Ho Man
      * Student ID: 1155077343
      * Email Address: hmliu6@cse.cuhk.edu.hk


       IDENTIFICATION DIVISION.
       PROGRAM-ID.   MAIN.
       AUTHOR.       LIU Ho Man.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT TEAMS ASSIGN TO 'teams.txt'
        ORGANIZATION IS LINE SEQUENTIAL.
       SELECT SUBMISSION ASSIGN TO 'submission-records.txt'
        ORGANIZATION IS LINE SEQUENTIAL.
       SELECT FILE-CONTEST-OUT ASSIGN TO 'reportcob.txt'
        ORGANIZATION IS LINE SEQUENTIAL
        ACCESS IS SEQUENTIAL.
       SELECT FILE-REPORT-OUT ASSIGN TO 'reportcob.txt'
        ORGANIZATION IS LINE SEQUENTIAL
        ACCESS IS SEQUENTIAL.
       SELECT FILE-RESULT-OUT ASSIGN TO 'reportcob.txt'
        ORGANIZATION IS LINE SEQUENTIAL
        ACCESS IS SEQUENTIAL.

       DATA DIVISION.
          FILE SECTION.
          FD TEAMS.
          01 TEAMS-FILE.
             05 TEAM-NAME           PIC A(15).

          FD SUBMISSION.
          01 SUBMISSION-RECORDS-FILE.
             05 RECORD-TEAM-NAME    PIC A(15).
             05 PROBLEM-ID          PIC 9(1).
             05 OUTCOME             PIC A(19).
             05 SCORE               PIC 9(3).

          FD FILE-CONTEST-OUT.
          01 CONTEST-TITLE.
             05 CONTEST-NAME        PIC A(34) VALUE X'0D'.

          FD FILE-REPORT-OUT.
          01 REPORT-TITLE.
             05 REPORT-NAME         PIC A(18) VALUE X'0D'.

          FD FILE-RESULT-OUT.
          01 TEAM-RESULT.
             05 STRING-RESULT       PIC A(93) VALUE X'0D'.

          WORKING-STORAGE SECTION.
          01 SWTICHES.
             05 EOF-TEAMS           PIC X VALUE 'N'.
             05 EOF-SUBMISSION      PIC X VALUE 'N'.

          01 POINTERS.
             05 TEAM-POINTER        PIC A(15) VALUE '               '.
             05 CURRENT-TEAM-RECORD PIC A(15) VALUE '               '.
             05 CURRENT-TEAM-PID    PIC 9(1) VALUE 0.

          01 INPUT-PARAMS.
             05 LAST-SCORE          PIC 9(3) VALUE 0.
             05 SUBMISSION-TIME     PIC 9(3) VALUE 0.
             05 SCORE-SUM           PIC 9(5) VALUE 0.
             05 MAX-SCORE           PIC 9(3) VALUE 0.
             05 MIN-SCORE           PIC 9(3) VALUE 999.

          01 PROBLEM.
             05 PROBLEM-SCORES      PIC 9(3) VALUE 0.
             05 DECAY               PIC 99v99 VALUE ZERO.
             05 ROBUSTNESS          PIC 9(3) VALUE 0.

          01 TEAM-SCORE.
             05 TOTAL-SCORE         PIC 9(4) VALUE 0.
             05 TEAM-PROBLEM        PIC 9(1) VALUE 0.
             05 TEAM-OUTPUT         PIC A(93).
             05 STRING-POINTER      PIC 9(2) VALUE 1.
             05 OUTPUT-SCORE        PIC 9(3) VALUE 0.
             05 FILL-UP             PIC X VALUE 'N'.
             05 DONE                PIC X VALUE 'N'.

          01 TEMP-VARIABLES.
             05 TOTAL-PARSING       PIC ZZZ9.
             05 SCORE-PARSING       PIC ZZ9.

       PROCEDURE DIVISION.
       MAIN-PARAGRAPH.
          PERFORM 100-INITIALIZE-TWO-FILES.
          PERFORM 200-READ-FROM-TEAMS.
          PERFORM 300-TITLE-FORMAT.
          PERFORM 600-TITLE-WRITE.
          PERFORM SUB-PARAGRAPH.
          PERFORM 900-TERMINATE.
          STOP RUN.

      *==========================================
      *READ ALL ROWS FROM TEAMS.TXT AND PRINT OUT
       SUB-PARAGRAPH.
          IF NOT EOF-TEAMS = 'Y' OR NOT EOF-SUBMISSION = 'Y' THEN
             IF NOT EOF-SUBMISSION = 'Y' THEN
                PERFORM 200-READ-FROM-SUBMISSION
      *         DISPLAY TEAM-OUTPUT
             END-IF

      *      DISPLAY TEAM-POINTER RECORD-TEAM-NAME

             IF (NOT EOF-TEAMS = 'Y' AND NOT TEAM-POINTER =
              RECORD-TEAM-NAME) OR (EOF-SUBMISSION = 'Y'
              AND NOT EOF-TEAMS = 'Y') THEN
      *         DISPLAY 'HERE'
                MOVE 'Y' TO FILL-UP
      *         DISPLAY 'DONE VALUE: ' DONE
                PERFORM 500-SCORE-CONCATENATION
                PERFORM 500-ADD-TOTAL-SCORE
                PERFORM 600-TEAM-RESULT-WRITE
                PERFORM 800-RESET-TEAM
                PERFORM 800-RESET-STRING
                PERFORM 200-READ-FROM-TEAMS
             END-IF

      *      IF TEAM-POINTER = RECORD-TEAM-NAME
      *       AND NOT EOF-SUBMISSION = 'Y' THEN
      *         DISPLAY 'STRING LENGTH: ' STRING-POINTER
      *         DISPLAY RECORD-TEAM-NAME PROBLEM-ID OUTCOME SCORE
      *      END-IF
             GO TO SUB-PARAGRAPH
          END-IF.
      *==========================================

      *===============================
      *100-FUNCTION ARE INITIALIZATION
       100-INITIALIZE-TWO-FILES.
          OPEN INPUT TEAMS.
          OPEN INPUT SUBMISSION.
          OPEN OUTPUT FILE-CONTEST-OUT.
      *===============================

      *==============================
      *200-FUNCTION ARE READ FUNCTION
       200-READ-FROM-TEAMS.
          READ TEAMS
             AT END
                MOVE 'Y' TO EOF-TEAMS
             NOT AT END
                MOVE TEAM-NAME TO TEAM-POINTER
      *         DISPLAY TEAM-NAME
                PERFORM 500-ADD-TEAM-NAME
          END-READ.

       200-READ-FROM-SUBMISSION.
          READ SUBMISSION
             AT END
                MOVE 'Y' TO EOF-SUBMISSION
                PERFORM 400-SCORE-CALCULATION
                PERFORM 800-RESET-VARIABLES
             NOT AT END
                IF CURRENT-TEAM-PID NOT = PROBLEM-ID THEN
      *            DISPLAY 'CURRENT POINTERS ID: ' CURRENT-TEAM-PID
      *            DISPLAY 'PROBLEM ID: ' PROBLEM-ID
                   PERFORM 400-SCORE-CALCULATION
                   PERFORM 800-RESET-VARIABLES
                   MOVE PROBLEM-ID TO CURRENT-TEAM-PID
                END-IF
                PERFORM 400-INPUT-PASSING
          END-READ.
      *==============================

      *==================================
      *300-FUNCTION ARE HARD-CODED STUFFS
       300-TITLE-FORMAT.
          MOVE '2018 CUHK CSE Programming Contest' TO CONTEST-NAME.
          MOVE 'Team Score Report' TO REPORT-NAME.
      *==================================

      *=========================================
      *400-FUNCTION ARE MATHEMATICAL CALCULATION
       400-INPUT-PASSING.
          ADD 1 TO SUBMISSION-TIME.
          ADD SCORE TO SCORE-SUM.
          MOVE SCORE TO LAST-SCORE.
          IF SCORE > MAX-SCORE THEN
             MOVE SCORE TO MAX-SCORE
          END-IF.
          IF SCORE < MIN-SCORE THEN
             MOVE SCORE TO MIN-SCORE
          END-IF.

       400-SCORE-CALCULATION.
          COMPUTE ROBUSTNESS = 100 - MAX-SCORE + MIN-SCORE.
          COMPUTE DECAY = 1 / SUBMISSION-TIME.
          IF MAX-SCORE <= 30 THEN
             MOVE 0 TO ROBUSTNESS
          END-IF.
          IF LAST-SCORE = 100 THEN
             MOVE 1 TO DECAY
          END-IF.
          COMPUTE PROBLEM-SCORES = 0.6 * LAST-SCORE
           + 0.3 * SCORE-SUM / SUBMISSION-TIME + 0.1 * ROBUSTNESS.
          IF LAST-SCORE NOT = 100 THEN
             COMPUTE PROBLEM-SCORES = 0.6 * LAST-SCORE * DECAY
              + 0.3 * SCORE-SUM / SUBMISSION-TIME
              + 0.1 * ROBUSTNESS
          END-IF.
      *   DISPLAY PROBLEM-SCORES.
      *   DISPLAY '--------------------'.
          ADD PROBLEM-SCORES TO TOTAL-SCORE.
          PERFORM 500-SCORE-CONCATENATION.
      *=========================================

      *================================
      *500-FUNCTION ARE STRING PARSING
       500-ADD-TEAM-NAME.
          STRING
             TEAM-NAME DELIMITED BY SIZE
             INTO TEAM-OUTPUT
             WITH POINTER STRING-POINTER
      *      ON OVERFLOW DISPLAY 'OVERFLOW!'
          END-STRING.

       500-SCORE-CONCATENATION.
          IF DONE = 'N' THEN
             PERFORM 500-ADD-PROBLEM-SCORE
          END-IF.
          IF (TEAM-PROBLEM <= CURRENT-TEAM-PID
           AND NOT FILL-UP = 'Y') AND DONE = 'N' THEN
             PERFORM 500-SCORE-CONCATENATION
          END-IF.
          IF (TEAM-PROBLEM <= 9 AND FILL-UP = 'Y') AND DONE = 'N' THEN
             PERFORM 500-SCORE-CONCATENATION
          END-IF.

       500-ADD-PROBLEM-SCORE.
          MOVE PROBLEM-SCORES TO OUTPUT-SCORE.
          IF TEAM-PROBLEM < CURRENT-TEAM-PID OR FILL-UP = 'Y' THEN
             MOVE 0 TO OUTPUT-SCORE
          END-IF.
          MOVE OUTPUT-SCORE TO SCORE-PARSING.
          STRING
             '(' DELIMITED BY SIZE
             TEAM-PROBLEM DELIMITED BY SIZE
             ')' DELIMITED BY SIZE
             SCORE-PARSING DELIMITED BY SIZE
             ' ' DELIMITED BY SIZE
             INTO TEAM-OUTPUT
             WITH POINTER STRING-POINTER
      *      ON OVERFLOW DISPLAY 'OVERFLOW!'
          END-STRING.
      *   DISPLAY TEAM-OUTPUT.
      *   DISPLAY '============='.
          IF TEAM-PROBLEM = 9 AND FILL-UP = 'Y' THEN
             MOVE 'N' TO FILL-UP
          END-IF.
          IF TEAM-PROBLEM = 9 THEN
             MOVE 'Y' TO DONE
          END-IF.
          IF TEAM-PROBLEM < 9 THEN
             ADD 1 TO TEAM-PROBLEM
          END-IF.

       500-ADD-TOTAL-SCORE.
          MOVE TOTAL-SCORE TO TOTAL-PARSING.
          STRING
             'T:' DELIMITED BY SIZE
             TOTAL-PARSING DELIMITED BY SIZE
             INTO TEAM-OUTPUT
             WITH POINTER STRING-POINTER
      *      ON OVERFLOW DISPLAY 'OVERFLOW!'
          END-STRING.
      *================================

      *================================
      *600-FUNCTION ARE FILE WRITING
       600-TITLE-WRITE.
      *   DISPLAY CONTEST-NAME.
      *   DISPLAY REPORT-NAME.
          WRITE CONTEST-TITLE.
          CLOSE FILE-CONTEST-OUT.
          OPEN EXTEND FILE-REPORT-OUT.
          WRITE REPORT-TITLE.
          CLOSE FILE-REPORT-OUT.
          OPEN EXTEND FILE-RESULT-OUT.

       600-TEAM-RESULT-WRITE.
      *   DISPLAY TEAM-OUTPUT.
          MOVE TEAM-OUTPUT TO STRING-RESULT.
          WRITE TEAM-RESULT.
      *================================


      *================================
      *800-FUNCTION ARE RESET VARIABLES
       800-RESET-VARIABLES.
          MOVE 0 TO LAST-SCORE.
          MOVE 0 TO SUBMISSION-TIME.
          MOVE 0 TO SCORE-SUM.
          MOVE 0 TO MAX-SCORE.
          MOVE 999 TO MIN-SCORE.

       800-RESET-TEAM.
          MOVE 0 TO TOTAL-SCORE.

       800-RESET-STRING.
          MOVE 'N' TO DONE.
          MOVE 'N' TO FILL-UP.
          MOVE '' TO TEAM-OUTPUT.
          MOVE 1 TO STRING-POINTER.
          MOVE 0 TO TEAM-PROBLEM.
      *================================

      *============================
      *900-FUNCTION ARE TERMINATION
       900-TERMINATE.
      *   DISPLAY 'NUMBER OF TEAM RECORDS >>>> ' TEAMS-COUNTER.
      *   DISPLAY 'NUMBER OF SUBMISSION RECORDS >>>> '
      *    SUBMISSION-COUNTER.
          CLOSE TEAMS.
          CLOSE SUBMISSION.
          CLOSE FILE-RESULT-OUT.
      *============================
