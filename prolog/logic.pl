% CSCI3180 Principles of Programming Languages

% --- Declaration ---

% I declare that the assignment here submitted is original except for source
% material explicitly acknowledged. I also acknowledge that I am aware of
% University policy and regulations on honesty in academic work, and of the
% disciplinary guidelines and procedures applicable to breaches of such policy
% and regulations, as contained in the website
% http://www.cuhk.edu.hk/policy/academichonesty/

% Assignment 4
% Name : Liu Ho Man
% Student ID : 1155077343
% Email Addr : hmliu6@cse.cuhk.edu.hk

/* 1a */
uint_num(0).
uint_num(s(X)) :- uint_num(X).

/* 1b */
gt(s(X), 0) :- !.
gt(s(X), s(Y)) :- gt(X, Y).


/* 1c */
/* Your query */

/* 1d */
sum(0, 0, Z) :- (Z = 0), !.
sum(0, s(Y), s(Z)) :- sum(0, Y, Z).
sum(s(X), 0, s(Z)) :- sum(X, 0, Z).
sum(s(X), s(Y), s(s(Z))) :- sum(X, Y, Z).

product(0, s(Y), Z) :- (Z = 0), !.
product(s(X), 0, Z) :- (Z = 0), !.
product(X, Y, Z) :- (X = s(A)), sum(M, Y, Z), product(A, Y, M), uint_num(X), uint_num(Y), uint_num(Z), !.

/* 1e */
% product(s(s(0)), s(s(s(0))), Z).

/* 1f */
% product(X, s(s(s(s(0)))), s(s(s(s(s(s(s(s(0))))))))).

/* 2a */
nth(X, [A | B], 1) :- (X = A), !.

nth(X, [A | B], N) :- (M is N - 1), nth(X, B, M).

/* 2b */
third(X, L) :- nth(X, L ,3).
