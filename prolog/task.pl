% CSCI3180 Principles of Programming Languages

% --- Declaration ---

% I declare that the assignment here submitted is original except for source
% material explicitly acknowledged. I also acknowledge that I am aware of
% University policy and regulations on honesty in academic work, and of the
% disciplinary guidelines and procedures applicable to breaches of such policy
% and regulations, as contained in the website
% http://www.cuhk.edu.hk/policy/academichonesty/

% Assignment 4
% Name : Liu Ho Man
% Student ID : 1155077343
% Email Addr : hmliu6@cse.cuhk.edu.hk

task(t1, 2, 4).
task(t2, 4, 6).
task(t3, 3, 7).
task(t4, 8, 9).
task(t5, 1, 10).
task(t6, 10, 1).


/* 1a */
check_task(T) :- task(T, Start_time, End_time), (End_time >= Start_time).

time_compatible(T1_Start, T1_End, T2_Start, T2_End) :-
    (   T1_Start > T2_Start
    ->  T1_Start >= T2_End
    ;   T2_Start >= T1_End).

/* 1b */
compatible(T1, T2) :- check_task(T1), check_task(T2), (task(T1, T1_Start, T1_End), task(T2, T2_Start, T2_End), time_compatible(T1_Start, T1_End, T2_Start, T2_End)).

compatible_element_with_list(Element, [Head | Tail]) :- 
    length(Tail, Length),
    (   Length > 0
    ->  check_task(Element), check_task(Head), compatible(Element, Head), compatible_element_with_list(Element, Tail)
    ;   check_task(Element), check_task(Head), compatible(Element, Head)).

/* 1c */
compatible_list([Head | Tail]) :- 
    length(Tail, Length),
    (   Length > 1
    ->  compatible_element_with_list(Head, Tail), compatible_list(Tail)
    ;   compatible_element_with_list(Head, Tail)).
