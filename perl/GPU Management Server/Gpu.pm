# CSCI3180 Principles of Programming Languages
# --- Declaration ---
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
# Assignment 3
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

use strict;
use warnings;

package Gpu;
use Task;
sub new {
    my $class = shift @_;
    my $id = shift @_;
    my $state = 0;
    my $self = {
        "time" => shift,
        "state" => $state,
        "task" => shift,
        "id" => $id,
    };
    bless $self, $class;
    return $self;
}
sub assign_task {
    my $self = shift @_;
    my $task = shift @_;

    $self->{"task"} = $task;
    $self->{"state"} = 1;
    $self->{"time"} = 0;
}
sub release {
    my $self = shift @_;

    $self->{"task"} = shift;
    $self->{"state"} = 0;
    $self->{"time"} = shift;
}
sub execute_one_time {
    my $self = shift @_;
    $self->{"time"} += 1;
}

sub id{
    my $self = shift @_;
    return $self->{"id"};
}

return 1;
