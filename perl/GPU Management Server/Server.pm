# CSCI3180 Principles of Programming Languages
# --- Declaration ---
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
# Assignment 3
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

use strict;
use warnings;
 
package Server;
use Gpu;
use Task;

our $task;
our $gpu;

sub new {
	my $class = shift @_;
    my $gpu_num = shift @_;
    my @gpus;
    my @waitq;
    for my $i (0 .. $gpu_num-1){
        my $gpuObject = Gpu->new($i);
        push @gpus, $gpuObject;
    }
    my $self = {
        "gpus" => \@gpus,
        "waitq" => \@waitq,
    };
    bless $self, $class;
    return $self;
}
sub task_info {
	return "task(user: ".$task->name().", pid: ".$task->pid().", time: ".$task->time().")";
}
sub task_attr {
	return $task->name(), $task->pid(), $task->time();
}
sub gpu_info {
	return "gpu(id: ".$gpu->id().")";
}
sub submit_task {
    my $self = shift @_;
    my $taskName = shift @_;
    my $taskTime = shift @_;
    local $task = Task->new($taskName, $taskTime);

    my $idleGPU = -1;
    for my $i (0 .. @{$self->{"gpus"}} - 1){
        # print $self->{"gpus"}->[$i]->{"state"}."\n";
        if ($self->{"gpus"}->[$i]->{"state"} == 0){
            $idleGPU = $i;
            last;
        }
    }
    if ($idleGPU > -1){
        $self->{"gpus"}->[$idleGPU]->assign_task($task);

        $task = $self->{"gpus"}->[$idleGPU]->{"task"};
        my $taskInfo = $self->task_info();

        $gpu = $self->{"gpus"}->[$idleGPU];
        my $gpuInfo = $self->gpu_info();

        print "$taskInfo => $gpuInfo"."\n";
    }
    else{
        push @{$self->{"waitq"}}, $task;

        $task = $self->{"waitq"}->[-1];
        my $taskInfo = $self->task_info();

        print "$taskInfo => waiting queue"."\n";
    }
}
sub deal_waitq {
    my $self = shift @_;
    for my $i (0 .. @{$self->{"gpus"}} - 1){
        if ($self->{"gpus"}->[$i]->{"state"} == 0 && @{$self->{"waitq"}} > 0){
            local $task = shift @{$self->{"waitq"}};
            $self->{"gpus"}->[$i]->assign_task($task);

            $task = $self->{"gpus"}->[$i]->{"task"};
            my $taskInfo = $self->task_info();

            $gpu = $self->{"gpus"}->[$i];
            my $gpuInfo = $self->gpu_info();

            print "$taskInfo => $gpuInfo"."\n";
        }
    }
}
sub kill_task {
    my $self = shift @_;
    my $killuser = shift @_;
    my $killPID = shift @_;
    my $found = 0;

    for my $i (0 .. @{$self->{"gpus"}} - 1){
        if($self->{"gpus"}->[$i]->{"task"}->pid() == $killPID){
            $found = 1;
            $task = $self->{"gpus"}->[$i]->{"task"};
            my $taskInfo = $self->task_info();
            if($self->{"gpus"}->[$i]->{"task"}->name() eq $killuser){
                $self->{"gpus"}->[$i]->release();
                print "user $killuser kill $taskInfo"."\n";
                $self->deal_waitq();
            }
            else{
                print "user $killuser kill task(pid: $killPID) fail"."\n";
            }
            last;
        }
    }

    if ($found == 0){
        for my $i (0 .. @{$self->{"waitq"}} - 1){
            if($self->{"waitq"}->[$i]->pid() == $killPID){
                $task = $self->{"waitq"}->[$i];
                my $taskInfo = $self->task_info();
                if($self->{"waitq"}->[$i]->name() eq $killuser){
                    splice(@{$self->{"waitq"}}, $i, 1);
                    print "user $killuser kill $taskInfo"."\n";
                }
                else{
                    print "user $killuser kill task(pid: $killPID) fail"."\n";
                }
                last;
            }
        }
    }
}
sub execute_one_time {
    my $self = shift @_;
    print "execute_one_time.."."\n";
    my $releasedGPU = 0;
    for my $i (0 .. @{$self->{"gpus"}} - 1){
        if ($self->{"gpus"}->[$i]->{"state"} == 1){
            $self->{"gpus"}->[$i]->execute_one_time();
            if ($self->{"gpus"}->[$i]->{"time"} >= $self->{"gpus"}->[$i]->{"task"}->{"time"}){
                print "task in gpu(id: $i) finished"."\n";
                $self->{"gpus"}->[$i]->release();
                $releasedGPU = 1;
            }
        }
    }
    
    if($releasedGPU == 1){
        $self->deal_waitq();
    }
}
sub show {
    my $self = shift @_;

    print "==============Server Message================"."\n";
    print "gpu-id  state  user  pid  tot_time  cur_time"."\n";
    # All running tasks in GPUs
    for my $i (0 .. @{$self->{"gpus"}} - 1){
        if ($self->{"gpus"}->[$i]->{"state"} == 1){
            my $state = "busy";
            $task = $self->{"gpus"}->[$i]->{"task"};
            my ($username, $taskPID, $tasktime) = $self->task_attr();
            my $currentTime = $self->{"gpus"}->[$i]->{"time"};


            print "  $i     $state   $username    $taskPID      $tasktime         $currentTime"."\n";
        }
        else{
            my $state = "idle";
            print "  $i     $state"."\n";
        }
    }

    # All tasks in waiting queue
    for my $i (0 .. @{$self->{"waitq"}} - 1){
        $task = $self->{"waitq"}->[$i];
        my ($username, $taskPID, $tasktime) = $self->task_attr();


        print "        wait   $username    $taskPID      $tasktime"."\n";
    }
    print "============================================"."\n\n";
}


return 1;