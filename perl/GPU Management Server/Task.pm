# CSCI3180 Principles of Programming Languages
# --- Declaration ---
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
# Assignment 3
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

use strict;
use warnings;

package Task;

our $pid = 0;

sub new {
    my $class = shift @_;
    my $name = shift @_;
    my $time = shift @_;
    my $self = {
        "pid" => $pid,
        "name" => $name,
        "time" => $time,
    };
    $pid += 1;
    bless $self, $class;
    return $self;
}

sub name{
    my $self = shift @_;
    return $self->{"name"};
}

sub pid{
    my $self = shift @_;
    return $self->{"pid"};
}

sub time{
    my $self = shift @_;
    return $self->{"time"};
}

return 1;