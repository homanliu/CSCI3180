### CSCI 3180 Assignment 3 Report

Liu Ho Man, 1155077343

#### Question 1: Dynamic Scoping in Task 2

```perl
our $task;

sub task_info {
	return "task(user: ".$task->name().", pid: ".$task->pid().", time: ".$task->time().")";
}

sub submit_task {
    ...
    local $task = Task->new($taskName, $taskTime);
    ...
    my $taskInfo = $self->task_info();
}
```

* In here, we used dynamic scoping for variable `$task` such that this variable can cover global value in local scope and use local value for calling `task_info()` without assigning other variables

#### Question 2: Dynamic Scoping vs Lexical Scoping

Lexical Scoping:

* Variables are only visible to current functions or its sub-functions

  ```perl
  # $taskName are not visible here
  sub submit_task {
      my $self = shift @_;
      my $taskName = shift @_;
      my $taskTime = shift @_;
      ...
  }
  ```

* Easy to read and trace the value of variables



Dynamic Scoping:

* Variables can be changed value temporarily without using additional variables in difference functions. When variable is called, it will recursively search for most closest value from caller.
* Not easy to trace back the value of variables
* Slower comparing with lexical scoping since compiler needs to keep tracking values of variables