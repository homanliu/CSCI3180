# CSCI3180 Principles of Programming Languages
# --- Declaration ---
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
# Assignment 3
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

use strict;
use warnings;

package Game;
use MannerDeckStudent; 
use Player;

sub new {
    my $class = shift;
    my $self = {
        "deck" => shift,
        "players" => shift,
        "cards" => shift,
    };
    bless $self, $class;
    return $self;
}

sub set_players {
    my $self = shift @_;
    $self->{"players"} = shift @_;
    my @players = @{$self->{"players"}};
    my $numOfPlayers = scalar(@players);
    if(52 % $numOfPlayers != 0){
        print "Error: cards' number 52 can not be divided by players number 3!"."\n";
        return 0;
    }
    print "There $numOfPlayers players in the game:"."\n";
    print join ' ', @players;
    print " \n\n";

    $self->{"deck"} = MannerDeckStudent->new();
    $self->{"deck"}->shuffle();
    $self->{"cards"} = [];
    my @carddeck = @{$self->{"deck"}->{"cards"}};
    
    # Split deck to all players and create player objects
    my @splittedDecks = $self->{"deck"}->AveDealCards(scalar(@players));
    for my $i (0..scalar(@players)-1){
        my $player = shift @{$self->{"players"}};
        my $playerObject = Player->new($player, \@{$splittedDecks[$i]});
        push @{$self->{"players"}}, $playerObject;
    }
    return 1;
}

sub getReturn {
    my $self = shift @_;

}

sub showCards {
    my $self = shift @_;
    my @cardStack = @{$self->{"cards"}};
    print join ' ', @cardStack;
    print "\n";
}

sub start_game {
    my $self = shift @_;
    my $numOfPlayers = scalar(@{$self->{"players"}});
    my $playerTurn = 0;
    my $numOfTurns = 0;
    my $gameRound = 1;
    print "Game begin!!!"."\n\n";

    while ($numOfPlayers > 1){
        $playerTurn = $numOfTurns % $numOfPlayers;

        my $currentPlayer = $self->{"players"}->[$playerTurn];
        my $currentPlayerName = $currentPlayer->{"name"};
        my $numOfPlayerCards = $currentPlayer->numCards();

        print "Player $currentPlayerName has $numOfPlayerCards cards before deal."."\n";
        # $currentPlayer->showCards();

        print "=====Before player's deal======="."\n";
        $self->showCards();
        print "================================"."\n";

        my $putCard = $currentPlayer->dealCards();
        # Check any repeated card
        my $sameCardOnStack = -1;
        for my $i (0 .. $#{$self->{"cards"}}){
            if ($self->{"cards"}->[$i] eq $putCard){
                $sameCardOnStack = $i;
                last;
            }
        }
        
        push @{$self->{"cards"}}, $putCard;
        my $stackSize = @{$self->{"cards"}};

        if ($putCard eq "J" and $stackSize > 1){
            my @cardsToPlayer;
            for my $i (0 .. $stackSize - 1){
                # my $element = shift @{$self->{"cards"}};
                my $tempSize = @{$self->{"cards"}};
                my $element = splice(@{$self->{"cards"}}, $tempSize - 1, 1);
                push @cardsToPlayer, $element;
            }
            $currentPlayer->getCards(\@cardsToPlayer);
        }
        elsif ($sameCardOnStack ne -1){
            # $sameCardOnStack index
            my @tempStack;
            for my $i (0 .. $sameCardOnStack - 1){
                my $element = shift @{$self->{"cards"}};
                push @tempStack, $element;
            }

            $stackSize = @{$self->{"cards"}};
            my @cardsToPlayer;
            for my $i (0 .. $stackSize - 1){
                # my $element = shift @{$self->{"cards"}};
                my $tempSize = @{$self->{"cards"}};
                my $element = splice(@{$self->{"cards"}}, $tempSize - 1, 1);
                push @cardsToPlayer, $element;
            }
            push @{$self->{"cards"}}, @tempStack;
            $currentPlayer->getCards(\@cardsToPlayer);
        }
        print "$currentPlayerName ==> card $putCard"."\n";

        # $currentPlayer->showCards();
        print "=====After player's deal======="."\n";
        $self->showCards();
        print "================================"."\n";

        $numOfPlayerCards = $currentPlayer->numCards();

        if ($playerTurn == $numOfPlayers - 1){
            $gameRound += 1;
        }

        if ($numOfPlayerCards == 0){
            my $removedPlayer = splice(@{$self->{"players"}}, $playerTurn, 1);
            my $removedPlayerName = $removedPlayer->{"name"};
            $numOfPlayers = scalar(@{$self->{"players"}});
            print "Player $removedPlayerName has 0 cards after deal."."\n";
            print "Player $removedPlayerName has no cards, out!"."\n\n";
        }
        else{
            print "Player $currentPlayerName has $numOfPlayerCards cards after deal."."\n\n";
        }

        $numOfTurns += 1;
    }
    my $winner = $self->{"players"}->[0];
    my $winnerName = $winner->{"name"};
    print "Winner is $winnerName in game $gameRound"."\n";
}

return 1;
