# CSCI3180 Principles of Programming Languages
# --- Declaration ---
# I declare that the assignment here submitted is original except for source
# material explicitly acknowledged. I also acknowledge that I am aware of
# University policy and regulations on honesty in academic work, and of the
# disciplinary guidelines and procedures applicable to breaches of such policy
# and regulations, as contained in the website
# http://www.cuhk.edu.hk/policy/academichonesty/
# Assignment 3
# Name : Liu Ho Man
# Student ID : 1155077343
# Email Addr : hmliu6@cse.cuhk.edu.hk

use strict;
use warnings;
 
package Player;
sub new {
    my $class = shift @_;
    my $name = shift @_;
    my @decks = shift @_;
    my $self = {
        "name" => $name,
        "decks" => @decks,
    };
    bless $self, $class;
    return $self;
}

sub getCards {
    my $self = shift @_;
    my $card = shift @_;
    push @{$self->{"decks"}}, @{$card};
}

sub dealCards {
    my $self = shift @_;
    my $popCard = shift @{$self->{"decks"}};
    return $popCard;
}

sub numCards {
    my $self = shift @_;
    my $numberOfCards = @{$self->{"decks"}};
    return $numberOfCards;
}

sub showCards {
    my $self = shift @_;
    my @cardStack = @{$self->{"decks"}};
    print join ' ', @cardStack;
    print "\n";
}

return 1;