#!bin/sh

perl GoldenHookFishing_sample1.pl > output1.txt
perl GoldenHookFishing_sample2.pl > output2.txt
perl GoldenHookFishing_sample3.pl > output3.txt

diff output1.txt output_sample1.txt
diff output2.txt output_sample2.txt
diff output3.txt output_sample3.txt

rm output1.txt output2.txt output3.txt
